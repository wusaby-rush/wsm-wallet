// vite.config.js
const { resolve } = require('path')
const { defineConfig } = require('vite')

module.exports = defineConfig({
  root:'src',
  build: {
    outDir: '../dist',
    rollupOptions: {
      input: {
        main: resolve(__dirname, 'src/index.html'),
        details: resolve(__dirname, 'src/pages/details.html'),
        entry: resolve(__dirname, 'src/pages/entry.html'),
        settings: resolve(__dirname, 'src/settings/index.html'),
        currency: resolve(__dirname, 'src/settings/currency.html'),
        contact: resolve(__dirname, 'src/settings/contact.html'),
        inventory: resolve(__dirname, 'src/pages/inventory.html'),
      }
    }
  }
})