import { addDoc, arrayUnion, collection, doc, serverTimestamp, setDoc } from 'firebase/firestore'
import { db } from '../main'
import { reactive } from 'petite-vue'

const entry = reactive({
    passive: true,
    currency: 'YER',
    type: 'purchase',
    description: '',
    timestamp: '',
    contacts: {},
    items: [],
    search: '',
    contact: {
        name: '',
        phone: '',
        address: '',
        note: ''
    },
    item: {
        name: '',
        price: 0,
        quantity: 0,
    },
    get total() {
        return this.items.reduce((total, item) => total + item.price * item.quantity, 0)
    },
    get searchResults () {
        const search = this.search.toLowerCase()
        return Object.values(this.contacts).filter(contact => {
            return contact.name.toLowerCase().includes(search) || contact.phone.toLowerCase().includes(search)
        })
    },
    now() {
        const now = new Date();
        now.setMinutes(now.getMinutes() - now.getTimezoneOffset());
        this.timestamp = now.toISOString().slice(0, 16)
    },
    resetContact() {
        this.contact.address = ''
        this.contact.note = ''
        document.dispatchEvent(new Event('toggleContact'))
    },
    resetItem() {
        this.item.name = ''
        this.item.price = 0
        this.quantity = 0
        document.dispatchEvent(new Event('toggleItem'))
    },
    addContact() {
        this.contact.name = this.search
        document.dispatchEvent(new Event('toggleContact'))
        // this.$emit('toggleContact')
    },
    choose(contact) {
        this.contact.name = contact.name
        this.contact.phone = contact.phone
        this.search = contact.name
    },
    saveContact() {
        if (!this.contact.name || !this.contact.phone) {
            console.error('name and phone are required')
            return
        }
        const profileRef = doc(db, 'users', localStorage.getItem('user'))
        const contactRef = doc(profileRef, 'contacts', this.contact.phone)
        try {
            // save contact if not existing
            setDoc(contactRef,{
                name: this.contact.name,
                address: this.contact.address,
                note: this.contact.note
            }, { merge: true })
        } catch (error) {
            console.error(error.message)
        } finally {
            this.search = this.contact.name
            this.reset()
        }
    },
    addItem() {
        if (!this.item.name || !this.item.price || !this.item.quantity) {
            console.error('name, price and quantity are required')
            return
        }

        this.items.push({
            name: this.item.name,
            price: this.item.price,
            quantity: this.item.quantity
        })

        this.item.name = ''
        this.item.price = 0
        this.item.quantity = 0
    },
    save(el) {
        
        // add entry
        const inventoryRef = collection(db, `inventory-${localStorage.getItem('user')}`)
        addDoc(inventoryRef, {
            items: this.items,
            createdAt: serverTimestamp(),
            type: this.type,
            currency: this.currency,
            description: this.description,
            timestamp: this.timestamp,
            total: this.total
        })
        .then(_ => {
            this.items = []
            this.description = ''
            this.now()
            //el.reset()
        })
        .catch(error => {
            console.error(error.message)
        })
    }
})

export default entry