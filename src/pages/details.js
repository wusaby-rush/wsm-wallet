import { reactive } from 'petite-vue'
import curency from '../settings/currency'

const details = reactive({
    account: 'general',
    items: [],
    contacts: {},
    get income() {
        let result = 0
        let items = this.items.filter(item => item.passive)
        items.forEach(item => {
            if (item.currency === 'USD')
                result += item.amount * curency.items.usd
            if (item.currency === 'SAR')
                result += item.amount * curency.items.sar 
            else
                result += item.amount
        })
        return result
    },
    get outcome() {
        let result = 0
        let items = this.items.filter(item => !item.passive)
        items.forEach(item => {
            if (item.currency === 'USD')
                result += item.amount * curency.items.usd
            if (item.currency === 'SAR')
                result += item.amount * curency.items.sar 
            else
                result += item.amount
        })
        return result
    }
})

export default details