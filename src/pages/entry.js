import { addDoc, arrayUnion, collection, doc, serverTimestamp, setDoc } from 'firebase/firestore'
import { db } from '../main'
import { reactive } from 'petite-vue'

const entry = reactive({
    passive: true,
    amount: 0,
    currency: 'YER',
    account: 'general',
    description: '',
    timestamp: '',
    contactDialog: false,
    items: {},
    search: '',
    name: '',
    phone: '',
    address: '',
    note: '',
    get searchResults () {
        const search = this.search.toLowerCase()
        return Object.values(this.items).filter(item => {
            return item.name.toLowerCase().includes(search) || item.phone.toLowerCase().includes(search)
        })
    },
    now() {
        const now = new Date();
        now.setMinutes(now.getMinutes() - now.getTimezoneOffset());
        this.timestamp = now.toISOString().slice(0, 16)
    },
    reset() {
        this.address = ''
        this.note = ''
        this.contactDialog = false
    },
    add() {
        this.name = this.search
        this.contactDialog = true
    },
    choose(contact) {
        this.name = contact.name
        this.phone = contact.phone
        this.search = contact.name
    },
    saveContact() {
        if (!this.name || !this.phone) {
            console.error('name and phone are required')
            return
        }
        const profileRef = doc(db, 'users', localStorage.getItem('user'))
        const contactRef = doc(profileRef, 'contacts', this.phone)
        try {
            // save contact if not existing
            setDoc(contactRef,{
                name: this.name,
                address: this.address,
                note: this.note
            }, { merge: true })
        } catch (error) {
            console.error(error.message)
        } finally {
            this.search = this.name
            this.reset()
        }
    },
    save(el) {
        // check data 
        if (!this.amount || !this.currency || !this.account || !this.phone) {
            console.error('all fields are required')
            return
        }
        // add entry
        const entryRef = collection(db, localStorage.getItem('user'))
        addDoc(entryRef, {
            passive: this.passive,
            amount: this.amount,
            currency: this.currency,
            account: this.account,
            description: this.description,
            timestamp: this.timestamp,
            createdAt: serverTimestamp(),
            phone: this.phone,
        })
        .then(_ => {
            this.amount = 0
            this.description = ''
            this.search = ''
            this.name = ''
            this.phone = ''
            this.now()
            //el.reset()
        })
        .catch(error => {
            console.error(error.message)
        })
    }
})

export default entry