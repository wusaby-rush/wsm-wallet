import { doc, deleteDoc, setDoc } from 'firebase/firestore'
import { db } from '../main'
import { reactive } from 'petite-vue'

const contact = reactive({
    contactDialog: false,
    edit: false,
    items: {},
    search: '',
    id: null,
    name: '',
    phone: '',
    address: '',
    note: '',
    get searchResults () {
        const search = this.search.toLowerCase()
        return Object.values(this.items).filter(item => {
            return item.name.toLowerCase().includes(search) || item.phone.includes(search)
        })
    },
    reset() {
        this.contactDialog = false
        this.edit = false
        this.id = null
        this.name = ''
        this.phone = ''
        this.address = ''
        this.note = ''
    },
    show(contact, id) {
        this.id = id
        this.name = contact.name
        this.phone = contact.phone
        this.address = contact.address
        this.note = contact.note
        this.contactDialog = true
    },
    remove() {
        const profileRef = doc(db, 'users', localStorage.getItem('user'))
        const contactRef = doc(profileRef, 'contacts', this.phone)
        try {
            deleteDoc(contactRef)
        } catch (error) {
            console.error(error.message)
        } finally {
            this.reset()
        }
    },
    save(el) {
        if (!this.name || !this.phone) {
            console.error('name and phone are required')
            return
        }
        const profileRef = doc(db, 'users', localStorage.getItem('user'))
        const contactRef = doc(profileRef, 'contacts', this.phone)
        try {
            // delete existing contact to update
            // if (this.id !== null) {
            //     console.log('remove existing contact', this.id)
            //     const old = this.items[this.id]
            //     updateDoc(profileRef, {
            //         contacts: arrayRemove({
            //             name: old.name,
            //             phone: old.phone,
            //             address: old.address,
            //             note: old.note
            //         })
            //     }, { merge: true })
            // }

            // save contact if not existing or update
            setDoc(contactRef,{
                name: this.name,
                address: this.address,
                note: this.note
            }, { merge: true })
        } catch (error) {
            console.error(error.message)
        } finally {
            this.reset()
            el.reset()
            this.contactDialog = false
        }
    }
})

export default contact