import { reactive } from 'petite-vue'
import { doc, setDoc } from 'firebase/firestore'
import { db } from '../main'

const currency = reactive({
    items: {},
    // save currencies to firestore db
    save(){
        const currenciesRef = doc(db, 'users', localStorage.getItem('user'))
        setDoc(currenciesRef, {
            currencies: this.items
        }, { merge: true })
    }
})

export default currency