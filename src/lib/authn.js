import {
    RecaptchaVerifier, 
    signInWithPhoneNumber,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    sendPasswordResetEmail,
    GoogleAuthProvider,
    signInWithRedirect,
    OAuthProvider,
    getRedirectResult,
    signOut,
} from 'firebase/auth'
import { auth } from '../main'

function phoneAuth() {
    return {
    phone: '+967',
    code: '',
    confirmResult: null,
    spinner: false,

    // phonen number verify
    async verifyPhoneNumber(el) {
        this.spinner = true
        try {
            const appVerifier = new RecaptchaVerifier('phone-signin', { size: 'invisible' }, auth)

            await signInWithPhoneNumber(auth, this.phone, appVerifier)
            .then(response => {
                this.confirmResult = response
            })
            .catch(error => {
                throw error
                grecaptcha.reset(appVerifier)
            })
        } catch (error) {
            console.error(error.message)
        } finally {
            this.spinner = false
            el.reset()
        }
    },

    async signInWithVerificationCode(el) {
        this.spinner = true
        try {
            await this.confirmResult.confirm(this.code)
            .then(cred => {
                // cred.user.uid
            })
        } catch (error) {
            console.error(error.message)
        } finally {
            el.reset()
            this.confirmResult = null
            this.spinner = false
        }
    }
}
}

function emailAuth() {
    return {
    email: '',
    password: '',
    confirmPassword: '',
    form: 'login',
    spinner: false,

    async emailSignUp(el) {
        this.spinner = true

        try {
            if (this.password !== this.confirmPassword)
            throw new "password/confirm doesnt match"

            await createUserWithEmailAndPassword(auth, this.email, this.password)
            .then(cred => {
                //cred.user.email
            })
        } catch (error) {
            console.error(error.message)
        } finally {
            this.spinner = false
            el.reset()
        }
    },

    async emailSignIn(el) {
        this.spinner = true
        
        try {
            await signInWithEmailAndPassword(auth, this.email, this.password)
            .then(cred => {
                // cred.user.email
            })
        } catch (error) {
            console.error(error.message)
        } finally {
            this.spinner = false
            el.reset()
        }
    },

    async emailReset(el) {
        this.spinner = true
        try {
            await sendPasswordResetEmail(auth, this.email)
            .then(() => {
                // Password reset email sent!
            })
        } catch (error) {
            console.error(error.message)
        } finally {
            this.spinner = false
            el.reset()
        }
    },

    providerSignIn(provider) {
        if (provider == 'apple')
            provider = new OAuthProvider('apple.com')
        else
            provider = new GoogleAuthProvider()

        signInWithRedirect(auth, provider)
        getRedirectResult(auth)
        .then((result) => {
            const credential = GoogleAuthProvider.credentialFromResult(result);
            const token = credential.accessToken;
            // The signed-in user info.
            const user = result.user;
            initProfile(user.uid)
        })
        .catch((error) => {
            console.error(error.message)
        })
    }
}
}

function endAuth() {
    return {
    signout() {
        if (!confirm('هل ترغب فعلا في تسجيل الخروج؟'))
            return
        signOut(auth)
    }
    }
}

export { phoneAuth, emailAuth, endAuth }