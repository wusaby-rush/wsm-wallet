import './style.css'

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app"
//import { getAnalytics } from "firebase/analytics"
import { getFirestore, enableIndexedDbPersistence, collection, doc, onSnapshot } from "firebase/firestore"
import { getAuth, onAuthStateChanged} from "firebase/auth"
import contact from './settings/contact'
import currency from './settings/currency'
import entry from './pages/entry'
import { reactive } from "petite-vue"
import details from './pages/details'
import inventory from './pages/inventory'

const store = reactive({
  get user() {
    return localStorage.getItem('user')
  },
  set user(value) {
    if (value) {
      localStorage.setItem('user', value)
    } else {
      localStorage.removeItem('user')
    }
  },
  set country(value) {
    if (value) {
      localStorage.setItem('country', value)
    }
    else {
      localStorage.removeItem('country')
    }
  },
  get country() {
    return localStorage.getItem('country')
  }
})

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDkEuy_t_d8MOFAmhNuZ_HEGzSJBeeRA40",
  authDomain: "wasl-wallet.firebaseapp.com",
  projectId: "wasl-wallet",
  storageBucket: "wasl-wallet.appspot.com",
  messagingSenderId: "113286089110",
  appId: "1:113286089110:web:bc1602077f068aa67ab5b7",
  measurementId: "G-L556D0S9K5"
};
  
  
// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
const db = getFirestore(firebase);
enableIndexedDbPersistence(db)
  .catch((err) => {
      if (err.code == 'failed-precondition') {
          alert("التطبيق مفتوح في اكثر من صفحة، فلن يعمل بدون اتصال انترنت")
      } else if (err.code == 'unimplemented') {
          console.erroe('indexedDB is not supported');
      }
  });
//getAnalytics(firebase);
const auth = getAuth(firebase);

// get user interactivly
onAuthStateChanged(auth, user => { // if logged out, user will be null
  if (!user) {
    store.user = null
      return
  }

  store.user = user.uid
  store.contact = user.email || user.phoneNumber   
})

if (store.user) {
  // get currencies
  const profileRef = doc(db, 'users', store.user)
  const unsubProfile = onSnapshot(profileRef, snapshot => {
    currency.items = snapshot.data().currencies
  }) 

  // get contacts
  const contactsRef = collection(profileRef, 'contacts')
  const unsubContactsRef = onSnapshot(contactsRef, snapshot => {
    const contacts = []
    snapshot.forEach(doc => {
      contacts.push({
        phone: doc.id,
        ...doc.data()
      })
      contact.items = contacts
      entry.items = contacts
      inventory.contacts = contacts
      let items = {}
      snapshot.docs.forEach(doc => {
        items[doc.id] = doc.data().name
      })
      details.contacts = items
    })
  })
}

// fetch('https://ipapi.co/country/', {mode:'cors'})
// .then(response => response.text())
// .then(country => {store.country = country})
localStorage.setItem('country', 'YE')

export { db, auth, store }